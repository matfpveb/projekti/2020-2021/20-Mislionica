export enum TestTokenType {
    Text,
    Field,
    Input,
    RadioButton
  }
  
  export class TestToken
  {
    constructor(public value : string, public type: TestTokenType ){}
  }
  export class TestTemplate
  {
      public tokens : TestToken[] = [];
      public answer! : TestToken;
  }