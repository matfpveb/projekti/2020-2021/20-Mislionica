import { Input, OnChanges, OnDestroy } from '@angular/core';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RegisterAlerts } from './models/register-alerts.model';
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { RequiredValidator, FullNameValidator, UserNameValidator, TeacherNameValidator, EmailValidator, PasswordValidator } from "../validators/login-register.validator";
import { AuthenticationService } from 'src/app/users/services/authentication.service';
import { Subscription } from 'rxjs';

interface IRegisterFormValue {
  fullname: string;
  username: string;
  teachername: string;
  email: string;
  grade: number;
  password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit, OnDestroy, OnChanges {
  @Input() isStudent!: boolean;

  registerAlerts: RegisterAlerts = new RegisterAlerts();
  regSub: Subscription = new Subscription();

  @ViewChild("fullnameInput") fullnameInput!: ElementRef;
  @ViewChild("usernameInput") usernameInput!: ElementRef;
  @ViewChild("teachernameInput") teachernameInput!: ElementRef;
  @ViewChild("emailInput") emailInput!: ElementRef;
  @ViewChild("passwordInput") passwordInput!: ElementRef;

  required_fullname:boolean = false;
  required_username:boolean = false;
  required_teachername:boolean = false;
  required_grade:boolean = false;
  required_email:boolean = false;
  required_password:boolean = false;

  registerForm : FormGroup;

  register_button : string = "Региструј се";

  constructor(private authService: AuthenticationService) { 
    this.registerForm = new FormGroup({
      fullname: new FormControl("", [Validators.required, RequiredValidator, FullNameValidator]),
      username: new FormControl("", [Validators.required, RequiredValidator, UserNameValidator]),
      teachername: new FormControl("", [Validators.required, RequiredValidator, TeacherNameValidator]),
      email: new FormControl("", [Validators.required, RequiredValidator, EmailValidator]),
      grade: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, RequiredValidator, PasswordValidator])
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.regSub ? this.regSub.unsubscribe() : null;
  }


  ngOnChanges(): void {
    if(this.isStudent)
    {
      this.resetSetingsForStudent();
      this.register_button = "Региструј се";
      this.registerAlerts.email_label = "Имејл адреса родитеља";
    }
    else
    {
      this.resetSetingsForTeacher();
      this.register_button = "Региструјте се";
      this.registerAlerts.email_label = "Имејл адреса";
    }
  }

  registerSubmit() : void 
  {
    if (this.checkValidation() !== false ) { return; }

    const data = this.registerForm.value as IRegisterFormValue;
    this.regSub = this.authService.registerUser(data.fullname, data.username, data.teachername, data.email, data.grade, data.password, this.isStudent).subscribe(()=>{window.location.reload();});
  }

  private checkValidation(): boolean 
  {
    let hasError: boolean = false;
    let hasErrorFullname: string = "";
    let hasErrorUsername: string = "";
    let hasErrorEmail: string = "";
    let hasErrorTeachername: string = "";
    let hasErrorPassword: string = "";
    let errors: string[] = [];

    const errorFullname : ValidationErrors | null = this.registerForm.get("fullname")!.errors;
    const errorUsername : ValidationErrors | null = this.registerForm.get("username")!.errors;
    const errorEmail : ValidationErrors | null = this.registerForm.get("email")!.errors;
    const errorTeachername : ValidationErrors | null = this.registerForm.get("teachername")!.errors;
    const errorPassword : ValidationErrors | null = this.registerForm.get("password")!.errors;

    this.required_fullname = errorFullname != null;
    this.required_username = errorUsername != null;
    this.required_teachername = errorTeachername != null;
    this.required_email = errorEmail != null;
    this.required_password = errorPassword != null;

    if (errorFullname != null){
      if (errorFullname.requiredValidator){
        hasErrorFullname = errorFullname.requiredValidator.message;
      } 

      if (errorFullname.fullNameValidator){
        this.registerAlerts.fullname_label = "*Име и презиме"
        window.alert(errorFullname.fullNameValidator.message);
      }
  
      this.registerAlerts.fullname_label = "*Име и презиме"
      hasError = true;
    } else {
        hasErrorFullname = "";
        this.registerAlerts.fullname_label = "Име и презиме";
        this.required_fullname = false;
    }

    if (errorUsername != null) {
      if (errorUsername.requiredValidator) {
        hasErrorUsername = errorUsername.requiredValidator.message;
      }

      if (errorUsername.usernamePartsValidator){
        this.registerAlerts.username_label = "*Корисничко име"
        window.alert(errorUsername.usernamePartsValidator.message);
      }

      if (errorUsername.usernameValidator){
        this.registerAlerts.username_label = "*Корисничко име"
        window.alert(errorUsername.usernameValidator.message);
      }

      this.registerAlerts.username_label = "*Корисничко име";
      hasError = true;
    } else {
      hasErrorUsername = "";
      this.registerAlerts.username_label = "Корисничко име";
      this.required_username = false;
    }

    if (errorTeachername != null && this.isStudent) {
      if (errorTeachername.requiredValidator) {
        hasErrorTeachername = errorTeachername.requiredValidator.message;
      }

      if (errorTeachername.teachernameValidator){
        this.registerAlerts.teachername_label = "*Име и презиме учитеља/учитељице";
        window.alert(errorTeachername.teachernameValidator.message);
      }

      this.registerAlerts.teachername_label = "*Име и презиме учитеља/учитељице";
      hasError = true;
    } else {
      hasErrorTeachername = "";
      this.registerAlerts.teachername_label = "Име и презиме учитеља/учитељице";
      this.required_teachername = false;
    }

    if (errorEmail != null) {
      if (errorEmail.requiredValidator){
        hasErrorEmail = errorEmail.requiredValidator.message;
      }

      if (errorEmail.emailValidator) {
        if(this.isStudent){ 
          this.registerAlerts.email_label = "*Имејл адреса родитеља";
        } else {
          this.registerAlerts.email_label = "*Имејл адреса";
        }
  
        window.alert(errorEmail.emailValidator.message);
      }

      if(this.isStudent){
        this.registerAlerts.email_label = "*Имејл адреса родитеља";
      } else {
        this.registerAlerts.email_label = "*Имејл адреса";
      }
      hasError = true;
    } else {
      hasErrorEmail = "";

      if(this.isStudent){
        this.registerAlerts.email_label = "Имејл адреса родитеља";
      } else {
        this.registerAlerts.email_label = "Имејл адреса";
      }

      this.required_email = false;
    }


    if (errorPassword != null) {
      if (errorPassword.requiredValidator){
        hasErrorPassword = errorPassword.requiredValidator.message;
      }

      if (errorPassword.passwordBlankoValidator){
        this.registerAlerts.password_label = "*Шифра";
        this.registerForm.get("password")!.patchValue("");
        window.alert(errorPassword.passwordBlankoValidator.message);
  
      }
  
      if (errorPassword.passwordValidator){
        this.registerAlerts.password_label = "*Шифра";
        this.registerForm.get("password")!.patchValue("");
        window.alert(errorPassword.passwordValidator.message);

      }

      this.registerAlerts.password_label = "*Шифра";
      hasError = true;
    } else {
      hasErrorPassword = "";
      this.registerAlerts.password_label = "Шифра";
      this.required_password = false;
    }

    if((this.registerForm.value.grade > 4 || this.registerForm.value.grade < 1)  && this.isStudent)
    {
      this.registerAlerts.grade_label = "*Разред";
      this.required_grade = true;
      hasError = true;
    }
    else
    {
      this.registerAlerts.grade_label = "Разред";
      this.required_grade = false;
    }

    errors.push(hasErrorFullname);
    errors.push(hasErrorUsername);
    errors.push(hasErrorTeachername);
    errors.push(hasErrorEmail);
    errors.push(hasErrorPassword);

    let newErrors = errors.filter((error) => error.length > 0);
    
    if (this.isStudent){
      if (newErrors.length != 0){
        this.registerAlerts.error_label = newErrors[0];
      } else {
        this.registerAlerts.error_label = "";
      }
    } else {
      if (newErrors.length <= 1){
        this.registerAlerts.error_label = "";
      } else {
        this.registerAlerts.error_label = newErrors[0];
      }
    }
    
    return hasError;
  }

  private resetSetingsForStudent(): void {
    this.required_password = false;
    this.required_username = false;
    this.required_fullname = false;
    this.required_teachername = false;
    this.required_grade = false;
    this.required_email = false;
    this.required_password = false;
    this.registerAlerts.error_label = "";
    this.registerAlerts.fullname_label = "Име и презиме";
    this.registerAlerts.teachername_label = "Име и презиме учитеља/учитељице";
    this.registerAlerts.email_label = "Имејл адреса родитеља";
    this.registerAlerts.password_label = "Шифра";
    this.registerAlerts.username_label = "Корисничко име"
    this.registerAlerts.grade_label = "Разред";
    this.registerForm.get("fullname")!.patchValue("");
    this.registerForm.get("username")!.patchValue("");
    this.registerForm.get("email")!.patchValue("");
    this.registerForm.get("teachername")!.patchValue("")
    this.registerForm.get("password")!.patchValue("");
  }

  private resetSetingsForTeacher(): void {
    this.required_password = false;
    this.required_username = false;
    this.required_fullname = false;
    this.required_email = false;
    this.required_password = false;
    this.registerAlerts.error_label = "";
    this.registerAlerts.fullname_label = "Име и презиме";
    this.registerAlerts.email_label = "Имејл адреса";
    this.registerAlerts.password_label = "Шифра";
    this.registerAlerts.username_label = "Корисничко име"
    this.registerForm.get("fullname")!.patchValue("");
    this.registerForm.get("username")!.patchValue("");
    this.registerForm.get("email")!.patchValue("");
    this.registerForm.get("password")!.patchValue("");
  }
}
