export class SubjectModel
{
    constructor(public subjectID: string, 
        public subjectName:string,public grade: number, public plan: string){}
}