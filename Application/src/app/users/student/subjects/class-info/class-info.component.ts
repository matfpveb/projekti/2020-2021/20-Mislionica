import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-info',
  templateUrl: './class-info.component.html',
  styleUrls: ['./class-info.component.css']
})
export class ClassInfoComponent implements OnInit {

  @Input() classInfoDataFromParent:string = "";

  constructor() {}

  ngOnInit(): void {
  }
}
