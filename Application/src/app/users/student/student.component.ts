import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model'
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit, OnDestroy {

  tabsStatus : Array<boolean> = [true,false,false,false];
  currentActiveTab : number = 0;
  private sub: Subscription;
  public user: User | null = null;
  is_visible: boolean = false;

  constructor(private auth: AuthenticationService) { 
    this.sub = this.auth.loginObsUser.subscribe(
      (user: User | null) => {this.user = user});

    this.auth.sendUserDataIfExists();
  }

  ngOnInit(): void {
    setInterval(() => {
      this.liveTime();
  }, 1000);
  }
  

  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }

  liveTime(): string
  {
    let cikaDate : Date = new Date();
    return (cikaDate.getHours() >=10 ? cikaDate.getHours().toString() : "0" + cikaDate.getHours().toString()) + ":" + 
    (cikaDate.getMinutes() >=10 ? cikaDate.getMinutes().toString() : "0" + cikaDate.getMinutes().toString()) + ":" + 
    (cikaDate.getSeconds() >=10 ? cikaDate.getSeconds().toString() : "0" + cikaDate.getSeconds().toString());
  }

  changeActiveTab(tab : number) : void
  {
    this.tabsStatus[this.currentActiveTab] = false;
    this.tabsStatus[tab] = true;
    this.currentActiveTab = tab;
  }

  getTabActiveStatus(tab : number) : string
  {
    return this.tabsStatus[tab] ? "active" : "hidden";
  }

  logout() : void
  {
    this.auth.logoutUser();
    window.location.reload();
  }

  viewPopup(event:any): void
  {
    event.preventDefault();
    this.is_visible = true;
  }

  cancelLogOut(event:any, whoPressed:number=1): void
  {
    if(whoPressed === 2)
    {
      this.is_visible = false;
    }
    if((event.target.className).includes("iskacilo") 
    || (event.target.className).includes("cd-popup-close"))
    {
      this.is_visible = false;
    }
  }
}
