import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentModel } from '../models/student.model';
import { AuthenticationService } from './authentication.service';
import { JwtService } from "../../common/services/jwt.service";
import { map,catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class StudentService {

  private students! : Observable<StudentModel[]>;

  private readonly getStudentsFromTeacherUrl = 'http://localhost:8000/api/students/teacher/';
  private readonly postStudent = "http://localhost:8000/api/students/update/";
  private readonly postUserProfileImageUrl = "http://localhost:8000/api/students/profile-image/";
  private readonly postStudentPassword = "http://localhost:8000/api/students/updatePassword/";

  constructor(private auth: AuthenticationService, private http:HttpClient) { 
  }

  getAllStudentsFromTeahcer(username : string) : Observable<StudentModel[]> {
    this.students = this.http.get<StudentModel[]>(this.getStudentsFromTeacherUrl + username);
    return this.students;
  }

  patchUserProfileImage(file: File | null, username:string|undefined): Observable<any> | null {

    if(file == null) {
      return null;
    }

    if(username === undefined)
    {
      return null;
    }

    const body: FormData = new FormData();
    body.append("file", file);

    return this.http.post<{ token: string }>(this.postUserProfileImageUrl+username, body).pipe(
      catchError((error: HttpErrorResponse) => this.auth.handleError(error)),
      map((response: { token: string | null }) => this.auth.mapResponseToUser(response)));
  };

  public postUserData(fullname: string, username: string, email: string, newUsername : string): Observable<any> | null {
    const body = { fullname, email, newUsername };

    return this.http
      .post<{ token: string }>(this.postStudent + username, body)
      .pipe(
        catchError((error: HttpErrorResponse) => this.auth.handleError(error)),
        map((response: { token: string | null }) => this.auth.mapResponseToUser(response))
      );
  };

  public postUserPassword(username: string, oldPassword: string, newPassword: string): Observable<any> | null {
    const body = { username, oldPassword, newPassword };

    return this.http
      .post<{ token: string }>(this.postStudentPassword, body)
      .pipe(
        catchError((error: HttpErrorResponse) => this.auth.handleError(error)),
        map((response: { token: string | null }) => this.auth.mapResponseToUser(response))
      );
  };
}
