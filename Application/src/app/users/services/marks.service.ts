import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { MarksModel } from '../models/marks.model';

@Injectable({
  providedIn: 'root'
})
export class MarksService 
{
  private marks! : Observable<MarksModel[]>;
  private readonly getStudentsFromTeacherUrl = 'http://localhost:8000/api/marks/';
  private readonly updateMarksForStudentUrl = 'http://localhost:8000/api/marks/update'

  constructor(private http:HttpClient) { }

  getMarksForStudent(username : string) : Observable<MarksModel[]>
  {
    this.marks = this.http.get<MarksModel[]>(this.getStudentsFromTeacherUrl+username);
    return this.marks;
  }

  updateMarksForStudent(username: string, marksChangeRequests: marksChangeRequest[]): Observable<any> | null 
  {
    if(!this.validateMarkInput(marksChangeRequests))
    {
      return null;
    }

    const body ={username : username, marksChangeRequests: marksChangeRequests};
    return this.http.post<{ token: string | null }>(this.updateMarksForStudentUrl, body);

  }

  validateMarkInput(marksChangeRequests : marksChangeRequest[]): boolean
  {
    if(marksChangeRequests.length === 0)
    {
      return false;
    }

    for(var i = 0; i < marksChangeRequests.length; ++i)
    {
      if(marksChangeRequests[i].semester !== "first" && marksChangeRequests[i].semester !== "second")
      {
        return false;
      }
      if(marksChangeRequests[i].mark > 5 || marksChangeRequests[i].mark < 1)
      {
        window.alert("Унели сте неважећу оцену : " + marksChangeRequests[i].mark);
        return false;
      }
    }
    return true;
  }
}

export class marksChangeRequest
{
  constructor(public subjectName: string,public mark : number, public semester:string, public isFinal : boolean, public changeTypeFlag:boolean){}
}