import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TestTemplate } from 'src/app/utils/testtoken';
import { TestModel } from '../models/test.model';
import { User } from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class TestsService 
{
    private readonly getTestUrl = 'http://localhost:8000/api/tests/';
    private readonly addTestUrl = 'http://localhost:8000/api/tests/add';
    private readonly submitTestUrl = 'http://localhost:8000/api/tests/submit';

    private test! : Observable<TestModel[]>;
    constructor(private http:HttpClient){}

    getTest(username : string) : Observable<TestModel[]>
    {
      const body = { username : username };
      this.test = this.http.post<TestModel[]>(this.getTestUrl, body);
      return this.test;
    }

    addTest(test : string, subjectID : string, grade : number)
    {
      const body = {testtemplate : test, subjectID: subjectID, grade:grade};
      this.http.post(this.addTestUrl, body).subscribe(data =>
        {
          window.alert("Тест је успешно направљен");
        });
    }

    submitTest(testHash : string,username : string, subjectID : string, id:string)
    {
      const body = {testtemplate : testHash, subjectID: subjectID, username:username, id:id};
      this.http.post(this.submitTestUrl, body).subscribe(data =>
        {
          window.alert("Тест је успешно послат");
        });
    }
}
