import mongoose from 'mongoose';

interface ITeacherDoc extends mongoose.Document
{
    _id: string,
    fullName: string,
    username: string,
    password: string,
    email: string,
    status: string,
    imgUrl: string
}

const teacherSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true
    },
    fullName: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    username: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    password: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    email: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    status: {
        type: mongoose.Schema.Types.String,
        default: "active"
    },
    imgUrl: {
        type: mongoose.Schema.Types.String,
        require: true,
        default: ""
    }
});

const Teachers = mongoose.model<ITeacherDoc>('teachers', teacherSchema);

export default Teachers;