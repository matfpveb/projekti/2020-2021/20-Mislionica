import mongoose from 'mongoose';

export interface ISubjectDoc extends mongoose.Document
{
    _id: string,
    subjectID: string,
    subjectName: string,
    grade: number,
    plan: string
}

const subjectsSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.String, 
        required: true
    },
    subjectID: { ///FORMAT : OSYID      primer: OS101 osnovna skola(OS) + razred(1) + redni broj predmeta(01)
        type: mongoose.Schema.Types.String,
        required: true
    },
    subjectName: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    grade: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    plan: {
        type: mongoose.Schema.Types.String,
        required: true
    }
});

const Subjects = mongoose.model<ISubjectDoc>('subjects', subjectsSchema);

export default Subjects;