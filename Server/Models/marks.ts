import mongoose from 'mongoose';
import { ISubjectDoc } from './subjects';

export interface IMarksDoc extends mongoose.Document
{
    _id: string,
    studentID: string,
    subjectID: string,
    mark: number,
    semester: string,
    isFinalMark: boolean
}

const marksSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true
    },
    //                             
    studentID: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    subjectID: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    mark: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    semester: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    isFinalMark: {
        type: mongoose.Schema.Types.Boolean,
        required: true,
        default: false
    },

},    {
    versionKey: false
    });

const Marks = mongoose.model<IMarksDoc>('marks', marksSchema);

export default Marks;