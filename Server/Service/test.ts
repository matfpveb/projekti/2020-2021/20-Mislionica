import mongoose, { mongo } from 'mongoose';
import ClassModel from '../Models/classes';
import Students from '../Models/students';
import SubmitedTestsModel from '../Models/submitedTests';
import TestModel from '../Models/tests';

export class Tests
{
    static async getTest() 
    {
        const tests =  await TestModel.findOne({});
        return tests;
    }

    static async getTestTemplateForClass() 
    {
        const template =  await ClassModel.find({}, { _id: 0, taskTemplate: 1});
        return template;
    }   

    static async addTest(testHash: string, subjectID : string, grade: number)
    {
        const newTest = new TestModel({ _id: new mongoose.Types.ObjectId(), subjectID, grade, testHash});  
        await newTest.save();
    };

    static async submitTest(testHash: string, subjectID : string, username: string, id: string)
    {
        const newTest = new SubmitedTestsModel({ _id: new mongoose.Types.ObjectId(), subjectID, username, testHash,testID: id});  
        await newTest.save();
    };

    static async getUnsubmitedTests(username : string) 
    {
        const student = await Students.findOne({username:username}).exec();
        if(student)
        {
            const tests = await TestModel.find({grade : student.grade}).exec();
            return tests;
        }
    }   
}