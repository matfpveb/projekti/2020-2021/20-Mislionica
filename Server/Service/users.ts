export abstract class User
{
    id: string;
    fullName: string;
    username: string; 
    email:string;
    password: string;

    constructor(id: string, fullName: string, username: string, 
                email:string, password: string)
    {
        this.id = id;
        this.fullName = fullName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    abstract toJSON():object;

    getId():string { return this.id; }
    getFullName():string { return this.fullName; }
    getUsername():string { return this.username; }
    getEmail():string { return this.email; }
    getPassword():string { return this.password; }
}