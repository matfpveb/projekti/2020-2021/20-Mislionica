import * as uuid from "uuid"
import bcrypt from 'bcrypt';
import mongoose, { mongo } from 'mongoose';

import * as jwt from '../Utils/jwt'
import {User} from "./users"
import Students from '../Models/students'

export class Student extends User
{
    teacher : string;
    grade : number;

    toJSON():object
    {
        return {
            id: this.id,
            fullName: this.fullName,
            username: this.username,
            teacher: this.teacher,
            grade: this.grade,
            email: this.email,
            password: this.password
        }
    }

    static async hashPassword(password)
    {
        const SALT_ROUNDS = 10;
        return await bcrypt.hash(password, SALT_ROUNDS);
    };

    static async getAllUsers()
    {
        const users = await Students.find({}).exec();
        return users;
    };

    static async getUserJWTByUsername(username:string) 
    {
        const user = await this.getUserByUsername(username);
        if (!user) {
          return null;
        }
        return jwt.generateJWT({
            _id: user.id,
            fullName: user.fullName,
            username: user.username,
            teacher: user.teacher,
            grade: user.grade,
            password: user.password,
            email: user.email,
            status: user.status,
            imgUrl: user.imgUrl
        });
    };

    static async getUserByUsername(username:string)
    {
        const user = await Students.findOne({ username: username }).exec();
        return user;
    };

    static async getUserByEmail(email:string)
    {
        const user = await Students.findOne({ email: email }).exec();
        return user;
    };
    
    static async getUsersByStatus(status:string)
    {
        const users = await Students.find({ status:status }).exec();
        return users;
    };

    static async getUsersByTeacher(teacher:string)
    {
        const users = await await Students.aggregate([
            {
                $lookup:
                {
                  from: "teachers",
                  let: { "teacherID": { "$toObjectId": "$teacher" } },
                  pipeline: [
                     { $match:
                        { $expr:
                           { $and:
                              [
                                { $eq: [ 
                                    "$_id",
                                    "$$teacherID"
                                ]},
                                { $eq: [ 
                                    "$username",
                                    teacher
                                ]},
                              ]
                           }
                        }
                     }
                  ],
                  as: "teachera"
                }
            },
            {
                $unwind: "$teachera" 
            },
            {
                $project:
                {
                    username: 1,
                    fullName: 1,
                }
            },

        ]).sort({"fullname":1}).exec();
        return users;
    };

    static async getUsersByGrade(grade:number)
    {
        const users = await Students.find({ grade:grade }).exec();
        return users;
    };

    static async addNewUser(fullName:string, username:string, email:string, password:string, 
        teacher:string, grade:number, status:string="active")
    {
        const cPassword = await this.hashPassword(password);
        const newUser = new Students({ _id: new mongoose.Types.ObjectId(), fullName, username, email, password: cPassword, teacher, grade, status });  
        await newUser.save();
        return Student.getUserJWTByUsername(username);
    };

    static async changeUserPassword(username:string, oldPassword:string, newPassword:string):Promise<object>
    {
        const user = await this.getUserByUsername(username);
        const matchPassword = await bcrypt.compare(oldPassword, user.password);

        if(!matchPassword)
        {
            return null;
        }
        
        const cPassword = await this.hashPassword(newPassword);
        await Students.findOneAndUpdate({ username: username },
                                        { $set: { password: cPassword } },
                                        { new: true });
        
        return Student.getUserJWTByUsername(username);
    };

    static async isUserPasswordValid(username, password)
    {
        const user = await this.getUserByUsername(username);
        return await bcrypt.compare(password, user.password);
    }

    static async deleteUser(username:string)
    {
        await Students.findOneAndDelete({ username: username }).exec();
    };

    static async changeProfileImage(username:string, imgUrl: string)
    {
        await Students.updateOne({ username: username },
            { $set: { imgUrl: imgUrl } });
    };

    static async updateUserData(fullname, username, email, newUsername) {
        await Students.updateOne({ username: username },
            { $set: { fullName: fullname, email: email, username: newUsername } });
    };
}