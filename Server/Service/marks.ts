import mongoose, { mongo } from 'mongoose';
import * as uuid from "uuid"

import MarksModule from '../Models/marks';
import Students from "../Models/students";
import SubjectModule from "../Models/subjects";

export class Marks
{
    static async getAllMarks():Promise<object>
    {
        const marks = await MarksModule.find({}).exec();
        return marks;
    };

    static async getFinalMarks():Promise<object>
    {
        const marks = await MarksModule.find({isFinalMark : true}).exec();
        return marks;
    };

    static async getMarksForStudent(username : string):Promise<object>
    {
        const marks = await MarksModule.aggregate([
        {

            $lookup:
            {
                from: "students",
                let: { "studentIDs": { "$toObjectId": "$studentID" } },
                pipeline: [
                    { $match:
                        { $expr:
                        { $and:
                            [
                                { $eq: [ 
                                    "$username",
                                    username
                                ]},
                                { $eq: [ 
                                    "$_id",
                                    "$$studentIDs"
                                ]},
                            ]
                        }
                        }
                    }
                ],
                as: "student"
            },
            
        },
        {
            $unwind: "$student"
        },
        {
            $lookup:
            {
                from: "subjects",
                localField: "subjectID",
                foreignField: "subjectID",
                as: "subjectNames"
            }
        },
        {
            $unwind: "$subjectNames"
        },
        ]).sort({ "mark": 1 }).exec();
        return marks;
    };

    static async addMarkForStudent(username:string,request:marksChangeRequest):Promise<object>
    {
        const student = await Students.findOne({username:username}).exec();
        if(student)
        {
            const subject = await SubjectModule.findOne({subjectName: request.subjectName, grade: student.grade }).exec();
            console.log(student.grade);
            console.log(subject);
            if(subject)
            {
                try{
                    if(request.isFinal)
                    {
                        const final = await MarksModule.findOne({subjectID:subject.subjectID,semester:request.semester,isFinalMark:request.isFinal,studentID:student._id}).exec();
                        if(final === null)
                        {
                            const newMark = new MarksModule({
                                _id : new mongoose.Types.ObjectId(), 
                                studentID: student._id, subjectID:subject.subjectID, mark: request.mark,semester:request.semester,isFinalMark:request.isFinal });  
                                await newMark.save();
                        }
                    }
                    else
                    {
                        const newMark = new MarksModule({
                            _id : new mongoose.Types.ObjectId(), 
                            studentID: student._id, subjectID:subject.subjectID, mark: request.mark,semester:request.semester,isFinalMark:request.isFinal });  
                            await newMark.save();
                    }
                } catch(error)
                {
                    console.log(error);
                }
            }
        }
        
        return student;
    };

    static async removeMarkForStudent(username:string,request:marksChangeRequest):Promise<object>
    {
        const student = await Students.findOne({username:username}).exec();
        if(student)
        {
            const subject = await SubjectModule.findOne({subjectName: request.subjectName, grade: student.grade}).exec();
            if(subject)
            {
                try{
                    await MarksModule.deleteOne
                    ({studentID: student._id, subjectID: subject.subjectID, mark: request.mark,semester:request.semester,isFinalMark:request.isFinal})
                    .exec();
                } catch(error)
                {
                    console.log(error);
                }
            }
        }
        return student;
    };


}

export class marksChangeRequest
{
  constructor(public subjectName: string,public mark : number, public semester:string, public isFinal : boolean, public changeTypeFlag:boolean){}
}