import express from "express"
import * as controller from '../../Controllers/classes'

const router = express.Router();

router.get('/:grade', controller.getAllClassesByGrade);
router.post('/class-image', controller.getClassImageByClassNumberSubjectIDAndGrade);

export default router;