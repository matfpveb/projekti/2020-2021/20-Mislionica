import { request } from "express";
import * as testService from "../Service/test";

const getTest = async (req, res, next) => {

    const username = req.body.username;
    try 
    {
      const allTests = await testService.Tests.getTest();
      res.status(200).json(allTests);
    } 
    catch (error) 
    {
      next(error);
    }
}

const getUnsubmitedTests = async (req, res, next) => {

  const username = req.body.username;
  try 
  {
    const allTests = await testService.Tests.getUnsubmitedTests(username);
    res.status(200).json(allTests);
  } 
  catch (error) 
  {
    next(error);
  }
}

const getTestTemplateForClass = async (req, res, next) => {

  try 
  {
    const template = await testService.Tests.getTestTemplateForClass();
    res.status(200).json(template);
  } 
  catch (error) 
  {
    next(error);
  }
}

const addTest = async (req, res, next) => {

  const {testtemplate, subjectID, grade} = req.body;

  try 
  {
    await testService.Tests.addTest(testtemplate,subjectID,grade);
    res.status(200).json();
  } 
  catch (error) 
  {
    next(error);
  }
}


const submitTets = async (req, res, next) => {
  const {testtemplate, subjectID, username, id} = req.body;
  try 
  {
    await testService.Tests.submitTest(testtemplate,subjectID,username, id);
    res.status(200).json();
  } 
  catch (error) 
  {
    next(error);
  }
}

export {
    getTest,
    getTestTemplateForClass,
    addTest,
    submitTets,
    getUnsubmitedTests
};