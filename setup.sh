service mongod start;
echo "Do you want to import DBs? [y/n]"
read answer
if [ $answer == "y" ]
then
	cd Server
	npm run importDBs
	cd ..
fi

cd Server
echo "Running a setup script for the Server"
npm run setup
cd ..
echo "Runnig a setup script for the Application"
cd Application
npm run setup
echo "Set up for the Server and the Application is completed!"
