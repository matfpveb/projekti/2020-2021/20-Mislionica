# Project Mislionica

Project `Mislionica` is an application which represents an online classroom intended for children in primary schools from first to forth grades. In this application children can learn and practice three primary subjects (mathematics, mother lenguage serbian, world around us (1. and 2. grade), nature and society (3. and 4. grade)). This application should encourage children to work regularly and enjoy learning. This application will show that learnig can be easy and interesting too. We hope that children will love this and will enjoy in this!

## Used technologies:

- Angular
- Express/Node.js
- MongoDB
- TypeScript

## Developers

- [Tamara Jevtimijević, 261/2017](https://gitlab.com/tamaricajev)
- [Stefan Isailović, 29/2016](https://gitlab.com/Isa1997)
- [Aleksa Voštić, 168/2016](https://gitlab.com/AKile9V)
